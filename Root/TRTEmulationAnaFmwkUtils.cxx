#include "TRTEmulationAna/TRTEmulationAnaFmwk.h"

void TRTEmulationAnaFmwk::fillSLpHThists(const bool isHT, const bool isHTMB, const int layer,
					 const int bec, const int slayer, const TString lep) {
  switch ( bec ) {
    int abslayer;
  case -2: // ECC
    abslayer = getAbsEndCapSL(slayer,layer);
    histoStore()->fillTProfile(lep+"/h_pHT_v_SL_ECC",  abslayer,isHT,weight());
    histoStore()->fillTProfile(lep+"/h_pHTmb_v_SL_ECC",abslayer,isHTMB,weight());
    break;
  case -1: // BA
  case  1: // BA
    abslayer = getAbsBarrelSL(slayer,layer);
    histoStore()->fillTProfile(lep+"/h_pHT_v_SL_BA",  abslayer,isHT,weight());
    histoStore()->fillTProfile(lep+"/h_pHTmb_v_SL_BA",abslayer,isHTMB,weight());
    break;
  case 2: // ECA
    abslayer = getAbsEndCapSL(slayer,layer);
    histoStore()->fillTProfile(lep+"/h_pHT_v_SL_ECA",  abslayer,isHT,weight());
    histoStore()->fillTProfile(lep+"/h_pHTmb_v_SL_ECA",abslayer,isHTMB,weight());
    break;
  default:
    Info("fillSLpHThists()","Unexpected bec value (%i)",bec);
    break;
  }
}

int TRTEmulationAnaFmwk::getAbsBarrelSL(const int sl, const int layer) const {
  int abs_SL = sl;
  if ( layer > 0 )
    abs_SL += 19;
  if ( layer > 1 )
    abs_SL += 24;
  if ( layer > 2 || abs_SL > 72 )
    TRT::fatal("getAbsBarrelSL(): Layer information is very bad!");
  return abs_SL;
}

int TRTEmulationAnaFmwk::getAbsEndCapSL(const int sl, const int wheel) const {
  int abs_SL = sl;
  if ( wheel < 6 )
    abs_SL += 16 * wheel;
  else
    abs_SL += 96 + (wheel - 6) * 8;
  if ( abs_SL > 159 )
    TRT::fatal("getAbsEndCapSL(): Layer information is very bad!");
  return abs_SL;
}
