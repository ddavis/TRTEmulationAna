#ifndef TRTEmulationAna_TRTEmulationAnaFmwk_H
#define TRTEmulationAna_TRTEmulationAnaFmwk_H

#include "TRTFramework/TRTAnalysis.h"

class TRTEmulationAnaFmwk : public TRTAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:

  bool m_doElectrons;
  bool m_doMuons;

  // Tree *myTree; //!
  // TH1 *myHist; //!



public:
  // this is a standard constructor
  TRTEmulationAnaFmwk();
  virtual ~TRTEmulationAnaFmwk();

  void set_doElectrons() { m_doElectrons = true; }
  void set_doMuons()     { m_doMuons     = true; }
  
  // these are the functions inherited from TRTAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode execute();

  void loopOverTRTHits(const xAOD::TrackParticle *track, const TString lep);

  int  getAbsBarrelSL(const int sl, const int layer) const;
  int  getAbsEndCapSL(const int sl, const int wheel) const;
  void fillSLpHThists(const bool isHT, const bool isHTMB, const int layer,
		      const int bec, const int slayer, const TString lep);

  // this is needed to distribute the algorithm to the workers
  ClassDef(TRTEmulationAnaFmwk, 1);
};

#endif // TRTEmulationAna_TRTEmulationAnaFmwk_H
