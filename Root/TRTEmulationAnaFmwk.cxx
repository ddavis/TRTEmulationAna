#include "TRTEmulationAna/TRTEmulationAnaFmwk.h"
#include "TRTFramework/TRTIncludes.h"

// this is needed to distribute the algorithm to the workers
ClassImp(TRTEmulationAnaFmwk)



TRTEmulationAnaFmwk::TRTEmulationAnaFmwk()
: TRTAnalysis()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

  // eventually add a command line option but for now make them default true
  // m_doMuons     = false;
  // m_doElectrons = false;
  m_doMuons     = true;
  m_doElectrons = true;
}



TRTEmulationAnaFmwk::~TRTEmulationAnaFmwk()
{
  // Here you delete any memory you allocated during your analysis.
}


EL::StatusCode TRTEmulationAnaFmwk::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  TRTAnalysis::execute();  // Must Keep This Line

  // Remove Duplicate Events
  if ( isDuplicate() ) return EL::StatusCode::SUCCESS;

  // Require GRL
  if ( not passGRL() ) return EL::StatusCode::SUCCESS;

  // Remove Events with Zero Weight
  if ( hasZeroWeight() ) return EL::StatusCode::SUCCESS;

  // Your Code Goes Here
  //-------------------------------------------------

  auto electrons = lepHandler()->getSelectedElectrons();
  auto muons     = lepHandler()->getSelectedMuons();

  auto nElectrons = electrons.size();
  auto nMuons     = muons.size();
  if ( nElectrons == 0 && nMuons == 0 ) return EL::StatusCode::SUCCESS;

  if ( m_doMuons ) {
    for ( auto const& muon : muons ) {
      const xAOD::TrackParticle* track = lepHandler()->getTrack(muon);
      int nTRThits = track->auxdata<unsigned char>("numberOfTRTHits");
      if ( nTRThits > 0 )
	histoStore()->fillTH1F("mu/h_nTRThits",nTRThits,weight());
      loopOverTRTHits(track,"mu");
    }
  }

  if ( m_doElectrons ) {
    for ( auto const& electron : electrons ) {
      const xAOD::TrackParticle* track = lepHandler()->getTrack(electron);
      int nTRThits = track->auxdata<unsigned char>("numberOfTRTHits");
      if ( nTRThits > 0 )
	histoStore()->fillTH1F("el/h_nTRThits",nTRThits,weight());
      loopOverTRTHits(track,"el");
    }
  }
  
  histoStore()->fillTH1F( "h_NPV", NPV(), weight() );
  histoStore()->fillTH1F( "h_averageMu", averageMu(), weight() );

  histoStore()->fillTH1F("h_nElectrons",nElectrons,weight());
  histoStore()->fillTH1F("h_nMuons",    nMuons,    weight());  

  return EL::StatusCode::SUCCESS;
}



void TRTEmulationAnaFmwk::loopOverTRTHits(const xAOD::TrackParticle *track, const TString lep)
{
  // Use this function to loop over TRT hits
  // Remove this entire function if not needed.

  // Get measurements on track
  typedef std::vector<ElementLink<xAOD::TrackStateValidationContainer>> MeasurementsOnTrack;
  if (not track->isAvailable<MeasurementsOnTrack>("msosLink"))
    TRT::fatal("No MSOS called msosLink is available on track");
  
  const MeasurementsOnTrack& measurementsOnTrack =
    track->auxdata< MeasurementsOnTrack >("msosLink");
  
  auto eta = track->eta();
  
  // Loop over track hits
  for ( auto msos_itr: measurementsOnTrack ) {
    if (not msos_itr.isValid()) continue;

    // skip non-TRT hits
    const xAOD::TrackStateValidation *msos = *msos_itr;
    if( msos->detType() != 3 || msos->type() != 0 ) continue;

    // get drift circle
    if (not msos->trackMeasurementValidationLink().isValid()) continue;
    auto DriftCirc =  *(msos->trackMeasurementValidationLink());

    // check if HT is passed
    bool isHT    =  DriftCirc->auxdata<char>("highThreshold");
    bool isHTMB  = (DriftCirc->auxdata<unsigned int>("bitPattern") & 131072);

    int layer   = DriftCirc->auxdata<int>("layer");
    int bec     = DriftCirc->auxdata<int>("bec");
    int slayer  = DriftCirc->auxdata<int>("strawlayer");

    fillSLpHThists(isHT,isHTMB,layer,bec,slayer,lep);

    histoStore()->fillTProfile(lep+"/h_pHT_v_eta",eta,isHT,weight());
    histoStore()->fillTProfile(lep+"/h_pHTmb_v_eta",eta,isHTMB,weight());
    
    // Get other variables
    // ...
    
    // Fill whatever histograms
    // ...
  }
}
