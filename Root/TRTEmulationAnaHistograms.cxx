#include "TRTFramework/TRTIncludes.h"
#include "TRTEmulationAna/TRTEmulationAnaFmwk.h"

EL::StatusCode TRTEmulationAnaFmwk::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  histoStore()->createTH1F("h_NPV", 30, 0, 30, "; N_{PV}; Events");
  histoStore()->createTH1F("h_averageMu", 30, 0, 30, "; #LT#mu#GT; Events");

  histoStore()->createTH1F("h_nMuons",    10,0,10,";N muons; Events");
  histoStore()->createTH1F("h_nElectrons",10,0,10,";N electrons; Events");

  for ( auto const& lep : {TString("el"),TString("mu")} ) {
    histoStore()->createTH1F(lep+"/h_nTRThits",70,0,70,";TRT hits; Events");

    histoStore()->createTProfile(lep+"/h_pHT_v_SL_BA", 72,0,72,  ";Straw Layer; p_{HT}");
    histoStore()->createTProfile(lep+"/h_pHT_v_SL_ECC",159,0,159,";Straw Layer; p_{HT}");
    histoStore()->createTProfile(lep+"/h_pHT_v_SL_ECA",159,0,159,";Straw Layer; p_{HT}");

    histoStore()->createTProfile(lep+"/h_pHTmb_v_SL_BA", 72,0,72,  ";Straw Layer; p_{HT} MB");
    histoStore()->createTProfile(lep+"/h_pHTmb_v_SL_ECC",159,0,159,";Straw Layer; p_{HT} MB");
    histoStore()->createTProfile(lep+"/h_pHTmb_v_SL_ECA",159,0,159,";Straw Layer; p_{HT} MB");

    histoStore()->createTProfile(lep+"/h_pHT_v_eta",  76,-1.9,1.9,";#eta;p_{HT}");
    histoStore()->createTProfile(lep+"/h_pHTmb_v_eta",76,-1.9,1.9,";#eta;p_{HT} MB");
  }
  return EL::StatusCode::SUCCESS;
}
